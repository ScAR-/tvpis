﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyScript
{
	class SyntaxError : Exception
	{
		public int pos;
		public string actual;
	}

	class ExpectError : SyntaxError
	{
		public string expected;

		public override string Message {
			get {
				return string.Format("at {0} expected `{1}` but saw `{2}`", pos, expected, actual);
			}
		}
	}

	class UnexpectError : SyntaxError
	{
		public override string Message {
			get {
				return string.Format("at {0} unexpected `{1}`", pos, actual);
			}
		}
	}
}
