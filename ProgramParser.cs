﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyScript
{
    class ProgramParser : ParserBase
    {
        public ProgramParser(string s) : base(s, 0) { }
        readonly Regex identifier_regex = new Regex(@"^[_a-zA-Z][_a-zA-Z0-9]*", RegexOptions.Compiled | RegexOptions.CultureInvariant);

        string try_parse_identifier()
        {
            var m = identifier_regex.Match(s.Substring(p));
            if (m.Success)
            {
                p += m.Length;
                return m.Value;
            }
            return null;
        }

        public ProgramStatement parse()
        {
            var statements = new List<Statement>();
            for (; ; )
            {
                skipSpaces();
                if (cur == eof)
                {
                    break;
                }
                var statement = parse_statement();
                statements.Add(statement);
            }
            return new ProgramStatement { statements = statements.ToArray() };
        }

        Statement parse_statement()
        {
            skipSpaces();
            var start_p = p;
            var identifier = try_parse_identifier();
            if (identifier == null)
            {
                return parse_expression_statement();
            }

            skipSpaces();
            if (identifier == "if")
            {
                expect('(');
                var condition = parse_expression();
                expect(')');
                return new If { condition = condition, body = parse_block() };
            }
            else if (identifier == "while")
            {
                expect('(');
                var condition = parse_expression();
                expect(')');
                return new While { condition = condition, body = parse_block() };
            }
            else if (identifier == "return")
            {
                var expression = parse_expression();
                expect(';');
                return new Return { expression = expression };
            }
            else if (skipIf("=") && !skipIf('='))
            {
                var expression = parse_expression();
                expect(';');
                return new Assignment { name = identifier, expression = expression };
            }
            p = start_p;
            return parse_expression_statement();
        }
        ExpressionStatement parse_expression_statement()
        {
            var expression = parse_expression();
            expect(';');
            return new ExpressionStatement { expression = expression };
        }

        Expression parse_expression()
        {
            return parse_equality();
        }
        Expression parse_equality()
        {
            skipSpaces();
            Func<Expression> ch = parse_relational;
            var first = ch();
            for (; ; )
            {
                skipSpaces();
                if (skipIf("=="))
                {
                    first = new BinaryOperation { type = BinaryOpType.Equal, left = first, right = ch() };
                }
                else if (skipIf("!="))
                {
                    first = new BinaryOperation { type = BinaryOpType.NotEqual, left = first, right = ch() };
                }
                else
                {
                    break;
                }
            }
            return first;
        }

        Expression parse_relational()
        {
            skipSpaces();
            Func<Expression> ch = parse_additive;
            var first = ch();
            for (; ; )
            {
                skipSpaces();
                if (skipIf("<="))
                {
                    first = new BinaryOperation { type = BinaryOpType.LessEqual, left = first, right = ch() };
                }
                else if (skipIf(">="))
                {
                    first = new BinaryOperation { type = BinaryOpType.GreaterEqual, left = first, right = ch() };
                }
                else if (skipIf("<"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Less, left = first, right = ch() };
                }
                else if (skipIf(">"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Greater, left = first, right = ch() };
                }
                else
                {
                    break;
                }
            }
            return first;
        }
        Expression parse_additive()
        {
            skipSpaces();
            Func<Expression> ch = parse_multiplicative;
            var first = ch();
            for (; ; )
            {
                skipSpaces();
                if (skipIf("+"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Plus, left = first, right = ch() };
                }
                else if (skipIf("-"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Minus, left = first, right = ch() };
                }
                else
                {
                    break;
                }
            }
            return first;
        }
        Expression parse_multiplicative()
        {
            skipSpaces();
            Func<Expression> ch = parse_primary;
            var first = ch();
            for (; ; )
            {
                skipSpaces();
                if (skipIf("*"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Mult, left = first, right = ch() };
                }
                else if (skipIf("/"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Div, left = first, right = ch() };
                }
                else if (skipIf("%"))
                {
                    first = new BinaryOperation { type = BinaryOpType.Mod, left = first, right = ch() };
                }
                else
                {
                    break;
                }
            }
            return first;
        }

        Expression parse_primary()
        {
            return parse_primary_suffix(parse_primary_prefix());
        }

        Expression parse_primary_prefix()
        {
            skipSpaces();
            var identifier = try_parse_identifier();
            if (identifier != null)
            {
                if (identifier == "function")
                {
                    skipSpaces();
                    var arguments = new List<string>();
                    expect('(');
                    for (; ; )
                    {
                        skipSpaces();
                        if (skipIf(')'))
                        {
                            break;
                        }
                        var argument = try_parse_identifier();
                        if (argument == null)
                        {
                            unexpected();
                        }
                        arguments.Add(argument);
                        skipSpaces();
                        if (!skipIf(','))
                        {
                            skipSpaces();
                            expect(')');
                            break;
                        }
                    }
                    return new Function { arguments = arguments.ToArray(), body = parse_block() };
                }
                else
                {
                    return new Variable { name = identifier };
                }
            }

            if (isDecimalDigit(cur))
            {
                var start = p;
                while (isDecimalDigit(cur))
                {
                    p += 1;
                }
                return new Number { value = double.Parse(s.Substring(start, p - start)) };
            }
            if (skipIf('('))
            {
                var arg = parse_expression();
                skipSpaces();
                expect(')');
                return new Parenthesis { child = arg };
            }

            if (skipIf("\'\'\'"))
            {
                string cur_str = s.Substring(p, 3);
                string res = "";
                while ((cur_str != "\'\'\'"))
                {
                    res = res.Insert(res.Length, s[p].ToString());
                    p++;
                    cur_str = s.Substring(p, 3);
                }
                expect("\'\'\'");
                return MyString.Create(res, "\'\'\'");
            }

            if (skipIf('\''))
            {
                string res = "";
                while ((cur != '\''))
                {
                    if (cur == '\\')
                    {
                        if (s[p + 1] == '\'')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\'");
                        }
                        else if (s[p + 1] == '\"')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\"");
                        }
                        else if (s[p + 1] == '\\')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\\");
                        }
                        else if (s[p + 1] == '0')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\0");
                        }
                        else if (s[p + 1] == 'a')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\a");
                        }
                        else if (s[p + 1] == 'b')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\b");
                        }
                        else if (s[p + 1] == 'f')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\f");
                        }
                        else if (s[p + 1] == 'n')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\n");
                        }
                        else if (s[p + 1] == 'r')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\r");
                        }
                        else if (s[p + 1] == 't')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\t");
                        }
                        else if (s[p + 1] == 'u')
                        {
                            int code = int.Parse(s.Substring(p + 2, 4), System.Globalization.NumberStyles.HexNumber);
                            p += 6;
                            res = res.Insert(res.Length, char.ConvertFromUtf32(code));
                        }
                        else if (s[p + 1] == 'v')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\v");
                        }
                        else break;
                    }
                    else
                    {
                        res = res + s[p];
                        p++;
                    }
                }
                expect('\'');
                var a = MyString.Create(res, "\'");
                return MyString.Create(res, "\'");
            }

            if (skipIf('\"'))
            {
                string res = "";
                while ((cur != '\"'))
                {
                    if (cur == '\\')
                    {
                        if (s[p + 1] == '\'')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\'");
                        }
                        else if (s[p + 1] == '\"')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\"");
                        }
                        else if (s[p + 1] == '\\')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\\");
                        }
                        else if (s[p + 1] == '0')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\0");
                        }
                        else if (s[p + 1] == 'a')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\a");
                        }
                        else if (s[p + 1] == 'b')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\b");
                        }
                        else if (s[p + 1] == 'f')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\f");
                        }
                        else if (s[p + 1] == 'n')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\n");
                        }
                        else if (s[p + 1] == 'r')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\r");
                        }
                        else if (s[p + 1] == 't')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\t");
                        }
                        else if (s[p + 1] == 'u')
                        {
                            int code = int.Parse(s.Substring(p + 2, 4), System.Globalization.NumberStyles.HexNumber);
                            p += 6;
                            res = res.Insert(res.Length, char.ConvertFromUtf32(code));
                        }
                        else if (s[p + 1] == 'v')
                        {
                            p += 2;
                            res = res.Insert(res.Length, "\v");
                        }
                        else break;
                    }
                    else
                    {
                        res = res.Insert(res.Length, s[p].ToString());
                        p++;
                    }
                }
                expect('\"');
                return MyString.Create(res, "\"");
            }
            unexpected();
            return null;
        }
        Expression parse_primary_suffix(Expression left)
        {
            for (; ; )
            {
                skipSpaces();
                if (skipIf('['))
                {
                    left = new Index { expression = left, index = parse_expression() };
                    skipSpaces();
                    expect(']');
                }
                else if (skipIf('('))
                {
                    var arguments = new List<Expression>();
                    for (; ; )
                    {
                        skipSpaces();
                        if (skipIf(')'))
                        {
                            break;
                        }
                        arguments.Add(parse_expression());
                        skipSpaces();
                        if (!skipIf(','))
                        {
                            skipSpaces();
                            expect(')');
                            break;
                        }
                    }
                    left = new FunctionCall { function = left, arguments = arguments.ToArray() };
                }
                else
                {
                    return left;
                }
            }
        }
        Block parse_block()
        {
            skipSpaces();
            expect('{');
            var statements = new List<Statement>();
            for (; ; )
            {
                skipSpaces();
                if (skipIf('}'))
                {
                    break;
                }
                statements.Add(parse_statement());
            }
            return new Block { statements = statements.ToArray() };
        }
    }
}
