﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasyScript
{
	class PrintFunction : FunctionValue
	{
		public System.IO.TextWriter Out = Console.Out;

		public override Value Call(Value[] arguments) {
			Out.WriteLine(string.Join(", ", (object[])arguments));
			return NullValue.Value;
		}

		public override string ToString() {
			return "function print(){ ... }";
		}
	}

	class ImportFunctionDynamic : FunctionValue
	{
		public Dictionary<string, Value> interpreted;
		public string path = null;

		public ImportFunctionDynamic()
		{
			interpreted = new Dictionary<string, Value>();
		}

		public ImportFunctionDynamic(string _path)
		{
			path = _path;
			interpreted = new Dictionary<string, Value>();
		}

		public override Value Call(Value[] arguments)
		{
			if (arguments.Length != 1)
			{
				throw new WrongNumberOfArguments();
			}

			MyStringValue filename = arguments[0] as MyStringValue;
			if (filename == null)
			{
				throw new WrongNumberOfArguments();
			}

			string dir = this.path == null ? Directory.GetCurrentDirectory() : this.path;

			string path = Path.Combine(dir, filename.str).ToLower();
			Debug.Assert(path == Path.GetFullPath(path));
			
			if (interpreted.Keys.Contains(path))
			{
				return interpreted[path];
			}
			
			string code = File.ReadAllText(path);
			var p = new ProgramParser(code).parse();
			EvaluatorDynamic.ProgramEvaluator e1 = new EvaluatorDynamic.ProgramEvaluator();
			e1.State.Set("import", new ImportFunctionDynamic(Path.GetDirectoryName(path)));
			var myret = e1.Run(p);
			interpreted.Add(path, myret);
			return myret;
		}
	}

	class ImportFunctionVisitor: FunctionValue
	{
		public Dictionary<string, Value> interpreted;
		public string path = null;

		public ImportFunctionVisitor()
		{
			interpreted = new Dictionary<string, Value>();
		}

		public ImportFunctionVisitor(string _path)
		{
			path = _path;
			interpreted = new Dictionary<string, Value>();
		}

		public override Value Call(Value[] arguments)
		{
			if (arguments.Length != 1)
			{
				throw new WrongNumberOfArguments();
			}

			MyStringValue filename = arguments[0] as MyStringValue;
			if (filename == null)
			{
				throw new WrongNumberOfArguments();
			}

			string dir = this.path == null ? Directory.GetCurrentDirectory() : this.path;

			string path = Path.Combine(dir, filename.str).ToLower();
			Debug.Assert(path == Path.GetFullPath(path));

			if (interpreted.Keys.Contains(path))
			{
				return interpreted[path];
			}

			string code = File.ReadAllText(path);
			var p = new ProgramParser(code).parse();
			EvaluatorVisitor.ProgramEvaluator e1 = new EvaluatorVisitor.ProgramEvaluator();
			e1.State.Set("import", new ImportFunctionDynamic(Path.GetDirectoryName(path)));
			e1.Run(p);
			var myret = e1.LastReturn;
			interpreted.Add(path, myret);
			return myret;
		}
	}

	interface IProgramEvaluator
	{
		void Run(Statement s);
		IState State { get; }
	}


	interface IState
	{
		Value Get(string name);
		void Set(string name, Value value);
	}
}
