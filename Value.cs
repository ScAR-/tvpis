﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyScript;

namespace EasyScript
{
	partial interface Value { }

	partial class NullValue : Value
	{
		public static readonly NullValue Value = new NullValue();

		private NullValue() { }

		public override string ToString() {
			return "null";
		}
	}
	partial class NumberValue : Value
	{
		public double value;

		public static NumberValue Create(double value) {
			return new NumberValue { value = value };
		}

		public override string ToString() {
			return value.ToString();
		}
	}
	partial class BoolValue : Value
	{
		public static readonly BoolValue True = new BoolValue { value = true };
		public static readonly BoolValue False = new BoolValue { value = false };

		public static BoolValue Create(bool b) {
			return b ? BoolValue.True : BoolValue.False;
		}

		private BoolValue() { }

		public bool value;
		public override string ToString() {
			return value ? "true" : "false";
		}
	}
    partial class MyStringValue : Value
    {
        public string str;
        public string quote;

        public static MyStringValue Create(string _str, string _quote)
        {
            return new MyStringValue { str = _str, quote = _quote };
        }
        
        public override string ToString()
        {
            return string.Format("{0}", str);
            //return string.Format("{0}{1}{0}", quote, str);
        }
    }
	abstract partial class FunctionValue : Value
	{
		public abstract Value Call(Value[] arguments);
	}
}
