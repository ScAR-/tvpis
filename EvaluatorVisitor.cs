﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyScript.EvaluatorVisitor;


namespace EasyScript
{
	#region expression
	partial interface Expression : Node
	{
		T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev);
	}
	partial class Null : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Number : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Bool : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Function : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Variable : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Parenthesis : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class BinaryOperation : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class Index : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
	partial class FunctionCall : Expression
	{
		public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
	}
    partial class MyString : Expression
    {
        public T Accept<T>(EvaluatorVisitor.IExpressionVisitor<T> ev) { return ev.Visit(this); }
    }

    #endregion

	#region statement
	partial interface Statement : Node
	{
		void Accept(EvaluatorVisitor.IStatementVisitor sv);
	}
	partial class ProgramStatement : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class Assignment : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class ExpressionStatement : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class If : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class While : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class Return : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	partial class Block : Statement
	{
		public void Accept(EvaluatorVisitor.IStatementVisitor sv) { sv.Visit(this); }
	}
	#endregion

	#region value
	partial interface Value
	{
		T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv);
	}
	partial class NullValue : Value
	{
		public T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv) { return vv.Visit(this); }
	}
	partial class NumberValue : Value
	{
		public T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv) { return vv.Visit(this); }
	}
	partial class BoolValue : Value
	{
		public T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv) { return vv.Visit(this); }
	}
	abstract partial class FunctionValue : Value
	{
		public T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv) { return vv.Visit(this); }
	}
    partial class MyStringValue : Value
    {
        public T Accept<T>(EvaluatorVisitor.IValueVisitor<T> vv) { return vv.Visit(this); }
    }

    #endregion
}

namespace EasyScript.EvaluatorVisitor
{
	interface IExpressionVisitor<out T>
	{
		T Visit(Null v);
		T Visit(Number v);
		T Visit(Bool v);
		T Visit(Variable v);
		T Visit(Parenthesis v);
		T Visit(BinaryOperation v);
		T Visit(Function v);
		T Visit(Index v);
		T Visit(FunctionCall v);
        T Visit(MyString v);
	}
	interface IStatementVisitor
	{
		void Visit(ProgramStatement s);
		void Visit(Assignment s);
		void Visit(ExpressionStatement s);
		void Visit(If s);
		void Visit(While s);
		void Visit(Return s);
		void Visit(Block s);
	}
	interface IValueVisitor<out T>
	{
		T Visit(NullValue v);
		T Visit(NumberValue v);
		T Visit(BoolValue v);
		T Visit(FunctionValue v);
        T Visit(MyStringValue v);
	}
	class UserFunctionValue : FunctionValue
	{
		public string[] arguments;
		public Block body;
		public Scope scope;
		public State state;
		public IStatementEvaluator se;
		public override string ToString() {
			return (new Function { arguments = arguments, body = body }).ToString();
		}
		public override Value Call(Value[] args) {
			if (arguments.Length != args.Length) {
				throw new WrongNumberOfArguments();
			}
			var variables = new Dictionary<string, Value>(arguments.Length);
			for (int i = 0; i < arguments.Length; i++) {
				variables[arguments[i]] = args[i];
			}
			state.Push(new Scope {
				parent = scope,
				variables = variables,
			});
			se.Run(body);
			var ret = state.LastReturn;
			state.Pop();
			return ret;
		}
	}

	class Scope
	{
		public Scope parent;
		public Dictionary<string, Value> variables = new Dictionary<string, Value>();
		public Value Return = NullValue.Value;
		public bool AfterReturn = false;
	}

	class State : IState
	{
		List<Scope> stack = new List<Scope>();
		Scope scope;

		public State() {
			Push();
		}

		public void Push() {
			scope = new Scope();
			stack.Add(scope);
		}
		public void Push(Scope new_scope) {
			scope = new_scope;
			stack.Add(scope);
		}
		public void Pop() {
			stack.RemoveAt(stack.Count - 1);
			scope = stack[stack.Count - 1];
		}

		internal void SetReturn(Value value) {
			scope.Return = value;
			scope.AfterReturn = true;
		}

		public Value Get(string name) {
			for (Scope s = scope; s != null; s = s.parent) {
				Value value;
				if (s.variables.TryGetValue(name, out value)) {
					return value;
				}
			}
			throw new NameNotDefined { name = name };
		}

		public void Set(string name, Value value) {
			scope.variables[name] = value;
		}

		public Value LastReturn {
			get { return scope.Return; }
		}

		public bool AfterReturn {
			get { return scope.AfterReturn; }
		}

		public Scope Current { get { return scope; } }
	}

	class StatementEvaluator : IStatementVisitor, IStatementEvaluator
	{
		public State state;
		public IExpressionEvaluator ee;

		Value Calc(Expression expr) {
			return ee.Calc(expr);
		}
		bool ToBool(Value value) {
			return ee.ToBool(value);
		}

		public void Run(Statement s) {
			s.Accept(this);
		}

		bool NeedBreak { get { return state.AfterReturn; } }

		public void Visit(ProgramStatement program) {
			foreach (var statement in program.statements) {
				Run(statement);
				if (NeedBreak) {
					break;
				}
			}
		}

		public void Visit(Assignment ass) {
			state.Set(ass.name, Calc(ass.expression));
		}

		public void Visit(ExpressionStatement statement) {
			Calc(statement.expression);
		}

		public void Visit(If s) {
			if (ToBool(Calc(s.condition))) {
				Visit(s.body);
			}
		}

		public void Visit(While s) {
			while (ToBool(Calc(s.condition))) {
				Visit(s.body);
				if (NeedBreak) {
					break;
				}
			}
		}

		public void Visit(Return ret) {
			state.SetReturn(Calc(ret.expression));
		}

		public void Visit(Block block) {
			foreach (var statement in block.statements) {
				Run(statement);
				if (NeedBreak) {
					break;
				}
			}
		}
	}

	class EqualVisitor : IValueVisitor<bool>
	{
		public Value b;

		public bool Visit(NullValue a) {
			return true;
		}

		public bool Visit(NumberValue a) {
			return (a.value == ((NumberValue)b).value);
		}

		public bool Visit(BoolValue a) {
			return (a.value == ((BoolValue)b).value);
		}

		public bool Visit(FunctionValue a) {
			return (a == b);
		}

        public bool Visit(MyStringValue str)
        {
            throw new UnsupportedOpperandTypes();
        }
	}

	class LessVisitor : IValueVisitor<bool>
	{
		public Value b;

		public bool Visit(NullValue a) {
			throw new UnsupportedOpperandTypes();
		}

		public bool Visit(NumberValue a) {
			return a.value < ((NumberValue)b).value;
		}

		public bool Visit(BoolValue a) {
			return !a.value && ((BoolValue)b).value;
		}

		public bool Visit(FunctionValue a) {
			throw new UnsupportedOpperandTypes();
		}

	    public bool Visit(MyStringValue str)
	    {
            throw new UnsupportedOpperandTypes();
	    }
	}

	class ExpressionEvaluator : IExpressionVisitor<Value>, IExpressionEvaluator
	{
		public State state;
		public IStatementEvaluator se;
		EqualVisitor equal_visitor = new EqualVisitor();
		LessVisitor less_visitor = new LessVisitor();

		public Value Calc(Expression expression) {
			return expression.Accept(this);
		}

		public bool ToBool(Value v) {
			var vv = v as BoolValue;
			if (vv == null) {
				throw new BadCast();
			}
			return vv.value;
		}

		public Value Visit(Null v) {
			return NullValue.Value;
		}

		public Value Visit(Number number) {
			return NumberValue.Create(number.value);
		}

	    public Value Visit(MyString ms)
	    {
            var mystringValue = MyStringValue.Create(ms.str, ms.quote);
	        return mystringValue;
	    }

		public Value Visit(Variable variable) {
			return state.Get(variable.name);
		}

		public Value Visit(Parenthesis v) {
			return Calc(v.child);
		}

		double ToNumber(Value value) {
			var number = value as NumberValue;
			if (number == null) {
				throw new UnsupportedOpperandTypes();
			}
			return number.value;
		}

		public Value Visit(BinaryOperation op) {
			var a = Calc(op.left);
			var b = Calc(op.right);
			switch (op.type) {
				case BinaryOpType.Plus: return NumberValue.Create(ToNumber(a) + ToNumber(b));
				case BinaryOpType.Minus: return NumberValue.Create(ToNumber(a) - ToNumber(b));
				case BinaryOpType.Mult: return NumberValue.Create(ToNumber(a) * ToNumber(b));
				case BinaryOpType.Div: return NumberValue.Create(ToNumber(a) / ToNumber(b));
				case BinaryOpType.Mod: return NumberValue.Create(ToNumber(a) % ToNumber(b));
				case BinaryOpType.Equal: return BoolValue.Create(CalcEqual(a, b));
				case BinaryOpType.NotEqual: return BoolValue.Create(!CalcEqual(a, b));
				case BinaryOpType.Less: return BoolValue.Create(CalcLess(a, b));
				case BinaryOpType.Greater: return BoolValue.Create(CalcLess(b, a));
				case BinaryOpType.LessEqual: return BoolValue.Create(!CalcLess(b, a));
				case BinaryOpType.GreaterEqual: return BoolValue.Create(!CalcLess(a, b));
			}
			throw new NotImplementedException();
		}

		bool CalcEqual(Value a, Value b) {
			if (a.GetType() != b.GetType()) {
				return false;
			}
			equal_visitor.b = b;
			return a.Accept(equal_visitor);
		}

		bool CalcLess(Value a, Value b) {
			if (a.GetType() != b.GetType()) {
				throw new UnsupportedOpperandTypes();
			}
			less_visitor.b = b;
			return a.Accept(less_visitor);
		}

		public Value Visit(Function f) {
			return new UserFunctionValue {
				se = se,
				state = state,
				scope = state.Current,
				arguments = f.arguments,
				body = f.body,
			};
		}

		public Value Visit(Index v) {
			throw new NotImplementedException();
		}

		public Value Visit(FunctionCall v) {
			var function = Calc(v.function) as FunctionValue;
			if (function == null) {
				throw new NotCallable();
			}

			var n = v.arguments.Length;
			var arguments = new Value[n];
			for (var i = 0; i < n; i++) {
				arguments[i] = Calc(v.arguments[i]);
			}
			return function.Call(arguments);
		}
	}

	class ProgramEvaluator : IProgramEvaluator
	{
		State state;
		Value lastReturn;
		IExpressionEvaluator ee;
		IStatementEvaluator se;

		public ProgramEvaluator()
		{
			state = new State();
			state.Set("null", NullValue.Value);
			state.Set("true", BoolValue.True);
			state.Set("false", BoolValue.False);
			state.Set("print", new PrintFunction());
			state.Set("import", new ImportFunctionVisitor());

			var ee = new ExpressionEvaluator { state = state };
			var se = new StatementEvaluator { state = state, ee = ee };
			ee.se = se;
			this.ee = ee;
			this.se = se;
		}

		public Value LastReturn { get { return lastReturn; } }

		public IState State { get { return state; } }
		public void Run(Statement statement) {
			se.Run(statement);
			lastReturn = state.LastReturn;
		}
	}
}
