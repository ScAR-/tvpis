﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyScript
{
	class RuntimeError : Exception { }

	class UnsupportedOpperandTypes : RuntimeError { }

	class NotCallable : RuntimeError { }

	class BadCast : RuntimeError { }

	class WrongNumberOfArguments : RuntimeError { }

	class FileNotFound : RuntimeError { }

	class NameNotDefined : RuntimeError
	{
		public string name;
		public override string Message { get { return string.Format("`{0}` is not defined", name); } }
	}
}
