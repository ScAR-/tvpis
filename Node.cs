﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyScript
{
	interface Node { }

	partial interface Expression : Node { }

	partial interface Statement : Node { }

	interface IStatementEvaluator
	{
		void Run(Statement statement);
	}

	interface IExpressionEvaluator
	{
		Value Calc(Expression expression);
		bool ToBool(Value value);
	}

	public enum BinaryOpType
	{
		Plus,
		Minus,
		Mult,
		Div,
		Mod,
		Less,
		Greater,
		LessEqual,
		GreaterEqual,
		Equal,
		NotEqual,
		_Count,
	}

	#region literals

	partial class Number : Expression
	{
		public double value;

		public override string ToString() {
			return value.ToString();
		}
	}
	partial class Function : Expression
	{
		public string[] arguments;
		public Block body;

		public override string ToString() {
			return string.Format("function ({0}) {1}", String.Join(", ", arguments), body);
		}
	}
    partial class MyString : Expression
    {
        public string str;
        public string quote;

        public static MyString Create(string _str, string _quote)
        {
            return new MyString { str = _str, quote = _quote };
        }

        public override string ToString()
        {
            return "\'" + str.Replace("\\", "\\\\").Replace("\'", "\\\'") + "\'";
        }
    }
	#endregion

	#region expressions
	partial class Variable : Expression
	{
		public string name;

		public override string ToString() {
			return name;
		}
	}

	partial class Parenthesis : Expression
	{
		public Expression child;

		public override string ToString() {
			return string.Format("({0})", child);
		}
	}

	partial class BinaryOperation : Expression
	{
		public Expression left;
		public Expression right;
		public BinaryOpType type;

		string TypeToString() {
			switch (type) {
				case BinaryOpType.Plus: return "+";
				case BinaryOpType.Minus: return "-";
				case BinaryOpType.Mult: return "*";
				case BinaryOpType.Div: return "/";
				case BinaryOpType.Mod: return "%";
				case BinaryOpType.Less: return "<";
				case BinaryOpType.Greater: return ">";
				case BinaryOpType.LessEqual: return "<=";
				case BinaryOpType.GreaterEqual: return ">=";
				case BinaryOpType.Equal: return "==";
				case BinaryOpType.NotEqual: return "!=";
				default: throw new Exception();
			}
		}

		public override string ToString() {
			return string.Format("{0} {1} {2}", left, TypeToString(), right);
		}
	}

	partial class Index : Expression
	{
		public Expression expression;
		public Expression index;

		public override string ToString() {
			return string.Format("{0}[{1}]", expression, index);
		}
	}
	partial class FunctionCall : Expression
	{
		public Expression function;
		public Expression[] arguments;

		public override string ToString() {
			return string.Format("{0}({1})", function, string.Join(", ", (object[])arguments));
		}
	}
	#endregion

	#region statements
	partial class ProgramStatement : Statement
	{
		public Statement[] statements;

		public override string ToString() {
			return string.Join("", (object[])statements);
		}
	}
	partial class Assignment : Statement
	{
		public string name;
		public Expression expression;

		public override string ToString() {
			return string.Format("{0} = {1};\n", name, expression);
		}
	}
	partial class ExpressionStatement : Statement
	{
		public Expression expression;

		public override string ToString() {
			return string.Format("{0};\n", expression);
		}
	}
	partial class If : Statement
	{
		public Expression condition;
		public Block body;

		public override string ToString() {
			return string.Format("if ({0}) {1}\n", condition, body);
		}
	}
	partial class While : Statement
	{
		public Expression condition;
		public Block body;

		public override string ToString() {
			return string.Format("while ({0}) {1}\n", condition, body);
		}
	}
	partial class Return : Statement
	{
		public Expression expression;

		public override string ToString() {
			return string.Format("return {0};\n", expression);
		}
	}
	partial class Block : Statement
	{
		public Statement[] statements;

		public override string ToString() {
			return string.Format("{{\n{0}}}", string.Join("", (object[])statements));
		}
	}

	#endregion
}
