﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyScript.EvaluatorDynamic
{
	class ReturnAction : Exception { }

	class UserFunctionValue : FunctionValue
	{
		public string[] arguments;
		public Block body;
		public Scope scope;
		public State state;
		public ProgramEvaluator program;
		public override string ToString() {
			return (new Function { arguments = arguments, body = body }).ToString();
		}
		public override Value Call(Value[] args) {
			if (arguments.Length != args.Length) {
				throw new WrongNumberOfArguments();
			}
			var variables = new Dictionary<string, Value>();
			for (int i = 0; i < arguments.Length; i++) {
				variables[arguments[i]] = args[i];
			}
			state.Push(new Scope {
				parent = scope,
				variables = variables,
			});
			try {
				program.Run(body);
			}
			catch (ReturnAction) { }
			var ret = state.LastReturn;
			state.Pop();
			return ret;
		}
	}

	class Scope
	{
		public Scope parent;
		public Dictionary<string, Value> variables = new Dictionary<string, Value>();
		public Value Return = NullValue.Value;
	}

	class State : IState
	{
		List<Scope> stack = new List<Scope>();
		Scope scope;

		public State() {
			Push();
		}

		public void Push() {
			scope = new Scope();
			stack.Add(scope);
		}
		public void Push(Scope new_scope) {
			scope = new_scope;
			stack.Add(scope);
		}
		public void Pop() {
			stack.RemoveAt(stack.Count - 1);
			scope = stack[stack.Count - 1];
		}

		internal void SetReturn(Value value) {
			scope.Return = value;
		}

		public Value Get(string name) {
			for (Scope s = scope; s != null; s = s.parent) {
				Value value;
				if (s.variables.TryGetValue(name, out value)) {
					return value;
				}
			}
			throw new NameNotDefined { name = name };
		}

		public void Set(string name, Value value) {
			scope.variables[name] = value;
		}

		public Value LastReturn {
			get { return scope.Return; }
		}

		public Scope Current { get { return scope; } }
	}

	class ProgramEvaluator : IProgramEvaluator
	{
		Dictionary<string, Value> interpreted = new Dictionary<string, Value>();
		State state;

		public ProgramEvaluator() {
			state = new State();
			state.Set("null", NullValue.Value);
			state.Set("true", BoolValue.True);
			state.Set("false", BoolValue.False);
			state.Set("print", new PrintFunction());
			state.Set("import", new ImportFunctionDynamic());
		}

		public IState State { get { return state; } }

		#region Run

		public Value Run(ProgramStatement program) {
			foreach (var statement in program.statements) {
				try
				{
					Run(statement);
				}
				catch (ReturnAction)
				{
					return this.state.LastReturn;
				}
			}
			return NullValue.Value;
		}

		public void Run(Statement statement) {
			Run((dynamic)statement);
		}


		void Run(ExpressionStatement statement) {
			Calc(statement.expression);
		}
		void Run(If s) {
			var condition = ToBool(Calc(s.condition));
			if (condition) {
				Run(s.body);
			}
		}
		void Run(While s) {
			while (ToBool(Calc(s.condition))) {
				Run(s.body);
			}
		}
		void Run(Block block) {
			foreach (var statement in block.statements) {
				Run(statement);
			}
		}
		void Run(Assignment ass) {
			state.Set(ass.name, Calc(ass.expression));
		}
		void Run(Return ret) {
			state.SetReturn(Calc(ret.expression));
			throw new ReturnAction();
		}
		#endregion

		#region Calc
		Value Calc(Expression expression) {
			return Calc((dynamic)expression);
		}
		Value Calc(FunctionCall expression) {
			var _function = Calc(expression.function);
			var function = _function as FunctionValue;
			if (function == null) {
				throw new NotCallable();
			}
			var n = expression.arguments.Length;
			var arguments = new Value[n];
			for (var i = 0; i < n; i++) {
				arguments[i] = Calc(expression.arguments[i]);
			}
			return function.Call(arguments);
		}
		Value Calc(Variable variable) {
			return state.Get(variable.name);
		}
		Value Calc(Function f) {
			return new UserFunctionValue {
				program = this,
				state = state,
				scope = state.Current,
				arguments = f.arguments,
				body = f.body,
			};

		}
        Value Calc(MyString mystring)
        {
            return MyStringValue.Create(mystring.str, mystring.quote);
        }
		Value Calc(Number number) {
			return NumberValue.Create(number.value);
		}

		double ToNumber(Expression expression) {
			var value = Calc(expression) as NumberValue;
			if (value == null) {
				throw new UnsupportedOpperandTypes();
			}
			return value.value;
		}
		double ToNumber(Value value) {
			var number = value as NumberValue;
			if (number == null) {
				throw new UnsupportedOpperandTypes();
			}
			return number.value;
		}
		Value Calc(BinaryOperation op) {
			var a = Calc(op.left);
			var b = Calc(op.right);
			switch (op.type) {
				case BinaryOpType.Plus: return NumberValue.Create(ToNumber(a) + ToNumber(b));
				case BinaryOpType.Minus: return NumberValue.Create(ToNumber(a) - ToNumber(b));
				case BinaryOpType.Mult: return NumberValue.Create(ToNumber(a) * ToNumber(b));
				case BinaryOpType.Div: return NumberValue.Create(ToNumber(a) / ToNumber(b));
				case BinaryOpType.Mod: return NumberValue.Create(ToNumber(a) % ToNumber(b));
				case BinaryOpType.Less: return BoolValue.Create(CalcLess(a, b));
				case BinaryOpType.Greater: return BoolValue.Create(CalcLess(b, a));
				case BinaryOpType.LessEqual: return BoolValue.Create(!CalcLess(b, a));
				case BinaryOpType.GreaterEqual: return BoolValue.Create(!CalcLess(a, b));
				case BinaryOpType.Equal: return BoolValue.Create(CalcEqual(a, b));
				case BinaryOpType.NotEqual: return BoolValue.Create(!CalcEqual(a, b));
			}
			throw new NotImplementedException();
		}

		#region Equal

		bool CalcEqual(Value a, Value b) {
			if (a.GetType() != b.GetType()) {
				return false;
			}
			return CalcEqual((dynamic)a, (dynamic)b);
		}
		bool CalcEqual(NumberValue a, NumberValue b) {
			return a.value == b.value;
		}
        bool CalcEqual(MyStringValue a, MyStringValue b)
        {
            return ((a.str == b.str) && (a.quote == b.quote));
        }
		bool CalcEqual(BoolValue a, BoolValue b) {
			return a.value == b.value;
		}
		bool CalcEqual(NullValue a, NullValue b) {
			return true;
		}
		bool CalcEqual(FunctionValue a, FunctionValue b) {
			return a == b;
		}
		#endregion

		#region Less

		bool CalcLess(Value a, Value b) {
			if (a.GetType() != b.GetType()) {
				throw new UnsupportedOpperandTypes();
			}
			return CalcLess((dynamic)a, (dynamic)b);
		}
		bool CalcLess(NumberValue a, NumberValue b) {
			return a.value < b.value;
		}
		bool CalcLess(BoolValue a, BoolValue b) {
			return !a.value && b.value;
		}
		#endregion

		Value Calc(Parenthesis p) {
			return Calc(p.child);
		}

		bool ToBool(Value v) {
			var vv = v as BoolValue;
			if (vv == null) {
				throw new BadCast();
			}
			return vv.value;
		}

		#endregion
	}
}
