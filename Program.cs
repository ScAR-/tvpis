﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EasyScript
{
	class DebugProgramEvaluator
	{
		Dictionary<string, Value> tInterpreted = new Dictionary<string, Value>();
		void t(string s) {
			var sw = new Stopwatch();
			var program = parse(s);
			var evaluators = new IProgramEvaluator[] {
				new EvaluatorDynamic.ProgramEvaluator(),
				//new EvaluatorVisitor.ProgramEvaluator(),
			};
			string last_out = null;
			foreach (var evaluator in evaluators) {
				sw.Restart();
				var print_out = new System.IO.StringWriter();
				//(evaluator.State.Get("print") as PrintFunction).Out = print_out;
				evaluator.Run(program);
				sw.Stop();
				if (last_out != null) {
					if (last_out != print_out.ToString()) {
						throw new Exception();
					}
				}
				last_out = print_out.ToString();
				Console.WriteLine("--- {0} {1}", sw.Elapsed, evaluator.GetType());
			}
		}

		ProgramStatement parse(string s) {
			var program1 = new ProgramParser(s).parse();

			if (true) {
				var s1 = program1.ToString();

				var program2 = new ProgramParser(s1).parse();
				var s2 = program2.ToString();

				if (s1 != s2) {
					Console.WriteLine(s1);
					throw new Exception();
				}
			}

			return program1;
		}

		public void Debug() {
			//Debug1();
			//Debug2();
			debug3();
		}
		#region debug1
		public void Debug1()
        {
            t(@"print('qё\'\""\\\0\a\b\f\n\r\t\v\u0073\u000b');");
            t(@"print('''qё\'\""\\\0\a\b\f\n\r\t\v\u0073\u000b''');");
            t(@"print(""qё\'\""\\\0\a\b\f\n\r\t\v\u0073\u000b"");");
            t(@"print('''

\n

''');");
		}
		#endregion
		#region debug2
		public void Debug2() {
			t(@"
hang = function(){
	while (true){}
};
sum = function(a, b){ return a + b; };
print(sum(1, 2));

make_null = function(){};
print(make_null());

gcd = function(a, b){
	while (b != 0){
		r = a % b;
		a = b;
		b = r;
	}
	return a;
};
gcd2 = function(a, b) {
	if (b == 0) {
		return a;
	}
	return gcd2(b, a % b);
};

i = 0;
while (i < 10){
	j = 0;
	while (j < 10){
		print(i, j, gcd(i, j), gcd2(i, j));
		j = j + 1;
	}
	i = i + 1;
}

");
			t(@"
print(1);
if (true) { print(print);}
print(1, 2+3*2, true, false, null, );
print(1+2, 1-2, 1*2, 1/2, 1%2);
(function(){
	t=true;
	f=false;
	fn=function(){};
	n=null;

	print(1<2, 1>2, 1<=2, 1>=2);
	print(t<f, t>f, t<=f, t>=f);	
					  
	print(2==1, 2==t, 2==fn, 2==n);	
	print(2!=1, 2!=t, 2!=fn, 2!=n);
					  
	print(t==1, t==t, t==fn, t==n);	
	print(t!=1, t!=t, t!=fn, t!=n);
					  
	print(fn==1, fn==t, fn==fn, fn==n);	
	print(fn!=1, fn!=t, fn!=fn, fn!=n);

	print(n==1, n==t, n==fn, n==n);	
	print(n!=1, n!=t, n!=fn, n!=n);	
})();
a = 1;
b = a + 1;
d = print;
d(b);
print(a, b);

pr = function(qa){
	return function(qb){
		print(qa, qb);
	};
};
print(pr);
pr1 = pr(13);
print(pr1);
pr1(14);

ppr = function(qa){
	return function(qb){
		return function(qc){
			print(qa, qb, qc);
		};
	};
};
ppr(21)(22)(23);

Fix = function (F) {
	return (function (g) {
		return function (x) {
			return F(g(g))(x);
		};
	})(function (g) {
		return function (x) {
			return F(g(g))(x);
		};
	});
};
fibo = Fix(function(f) {
	return function (n) {
		if (n <= 1){
			return n;
		}		
		return f(n - 1) + f(n - 2);        
	};
});

fib1 = function(n){
	if (n <= 1){
		return n;
	}
	return fib1(n - 1) + fib1(n - 2);
};

fib2 = function(n){
	if (n == 0){
		return 0;
	}
	if (n != 1){
		return fib2(n - 1) + fib2(n - 2);
	}
	return 1;
};
 
i = 0;
while (i <= 16){
	print(i, fib1(i), fibo(i), fib2(i));
	i = i + 1;
}


");
		}
		#endregion
		public void debug3()
		{
			t(@"print(import('test.txt'));");
			t(@"print(import('gg/test2.txt'));");
		}
	}

	class Program
	{
		static void Main(string[] args) {
			new DebugProgramEvaluator().Debug();
		}
	}
}
