﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyScript
{
	class ParserBase
	{
		protected string s;
		protected const char eof = char.MaxValue;
		protected int p = 0;

		public int pos { get { return p; } }

		public ParserBase(string s, int p = 0) {
			this.s = s + eof;
			this.p = p;
		}

		protected char cur { get { return s[p]; } }

		protected bool isSpace(char c) {
			return c <= ' ';
		}
		protected bool isDecimalDigit(char c) {
			return '0' <= c && c <= '9';
		}
		protected bool skipIf(string ss) {
			if (string.Compare(s, p, ss, 0, ss.Length) == 0) {
				p += ss.Length;
				return true;
			}
			return false;
		}
		protected bool skipIf(char c) {
			if (s[p] == c) {
				p++;
				return true;
			}
			return false;
		}
		protected void unexpected() {
			throw new UnexpectError { pos = p, actual = s[p].ToString() };
		}
		protected void expect(char c) {
			if (s[p] != c) {
				throw new ExpectError { pos = p, expected = c.ToString(), actual = s[p].ToString() };
			}
			p++;
		}
        protected void expect(string ss)
        {
            if (string.Compare(s, p, ss, 0, ss.Length) != 0)
            {
                throw new ExpectError { pos = p, expected = ss, actual = s };
            }
            p+= ss.Length;
        }
		protected void skipSpaces() {
			for (; ; ) {
				if (s[p] == '/') {
					if (s[p + 1] == '*') {
						p += 2;
						while (!(s[p] == '*' && s[p + 1] == '/')) {
							if (s[p] == eof) {
								unexpected();
							}
							p += 1;
						}
						p += 2;
					}
					else if (s[p + 1] == '/') {
						p += 2;
						while (!(s[p] == '\n' || s[p] == eof)) {
							p += 1;
						}
					}
					else {
						break;
					}
				}
				else if (isSpace(s[p])) {
					p += 1;
				}
				else {
					break;
				}
			}
		}
	}
}
